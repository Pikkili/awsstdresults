package hudson.plugins.s3;
import java.io.File;
import java.io.IOException;
import java.util.Random;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Date;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;

public class UploadObjectSingleOperation {
	private static String bucketName     = "firstawsallusers";
	private static String keyName        = "StudentMarks" + new Date().getTime();
	private static String uploadFileName = "Tempawsfile.txt";
	
	public static void main(String[] args) throws IOException {
		BufferedWriter output=null;
		File filestd = new File("Tempawsfile.txt");
		output = new BufferedWriter(new FileWriter(filestd));
		
		int sci,mat,soc,eng,tel,hin;
		int tot,avg,i;
		boolean pass = false;
		sci=mat=soc=eng=tel=hin=avg=0;
		Random randomGenerator = new Random();
		for(i=0;i<6;i++)
		{
			if(i==0) sci = randomGenerator.nextInt(100);
			if(i==1) mat = randomGenerator.nextInt(100);
			if(i==2) eng = randomGenerator.nextInt(100);
			if(i==3) tel = randomGenerator.nextInt(100);
			if(i==4) hin = randomGenerator.nextInt(100);
			if(i==5) soc = randomGenerator.nextInt(100);
		}
		tot = sci+mat+eng+tel+hin+soc;
		if(sci >= 35 && mat >= 35 && eng >= 35 && tel >=35 && hin >=35 && soc >= 35)
		{
			avg = tot/6;
			pass = true;
		}
		
		if(avg >= 70 && avg < 100 && pass)
		{
			output.write("**************************************************************\n");
			output.write("Congratulations!!!\n");
			output.write("Distinction");
			output.write("\n**************************************************************\n");
			output.write("Here are you marks\n");
			output.write("Subject\t\tMarks\n");
			output.write("----------------------------\n");
			output.write("English : \t"+eng+"\n");
			output.write("Telugu  : \t"+tel+"\n");
			output.write("Hindi   : \t"+hin+"\n");
			output.write("Science : \t"+sci+"\n");
			output.write("Maths   : \t"+mat+"\n");
			output.write("Social  : \t"+soc+"\n");
		}
		else if(avg >=60 && avg < 70 && pass)
		{
			output.write("**************************************************************\n");
			output.write("Congratulations!!!\n");
			output.write("First class");
			output.write("\n**************************************************************\n");
			output.write("Here are you marks\n");
			output.write("Subject\t\tMarks\n");
			output.write("----------------------------\n");
			output.write("English : \t"+eng+"\n");
			output.write("Telugu  : \t"+tel+"\n");
			output.write("Hindi   : \t"+hin+"\n");
			output.write("Science : \t"+sci+"\n");
			output.write("Maths   : \t"+mat+"\n");
			output.write("Social  : \t"+soc+"\n");
		}
		else if(avg >= 50 && avg < 60 && pass)
		{
			output.write("**************************************************************\n");
			output.write("Congratulations!!!\n");
			output.write("Second class");
			output.write("\n**************************************************************\n");
			output.write("Here are you marks\n");
			output.write("Subject\t\tMarks\n");
			output.write("----------------------------\n");
			output.write("English : \t"+eng+"\n");
			output.write("Telugu  : \t"+tel+"\n");
			output.write("Hindi   : \t"+hin+"\n");
			output.write("Science : \t"+sci+"\n");
			output.write("Maths   : \t"+mat+"\n");
			output.write("Social  : \t"+soc+"\n");
		}
		else if(avg >= 35 && avg < 50 && pass)
		{
			output.write("**************************************************************\n");
			output.write("Congratulations!!!\n");
			output.write("Third class");
			output.write("\n**************************************************************\n");
			output.write("Here are you marks\n");
			output.write("Subject\t\tMarks\n");
			output.write("----------------------------\n");
			output.write("English : \t"+eng+"\n");
			output.write("Telugu  : \t"+tel+"\n");
			output.write("Hindi   : \t"+hin+"\n");
			output.write("Science : \t"+sci+"\n");
			output.write("Maths   : \t"+mat+"\n");
			output.write("Social  : \t"+soc+"\n");
		}
		else
		{
			output.write("**************************************************************\n");
			output.write("Sorry!!!\n");
			output.write("Fail");
			output.write("\n**************************************************************\n");
			output.write("Here are you marks\n");
			output.write("Subject\t\tMarks\n");
			output.write("----------------------------\n");
			output.write("English : \t"+eng+"\n");
			output.write("Telugu  : \t"+tel+"\n");
			output.write("Hindi   : \t"+hin+"\n");
			output.write("Science : \t"+sci+"\n");
			output.write("Maths   : \t"+mat+"\n");
			output.write("Social  : \t"+soc+"\n");
		}
		output.close();
		if ( output != null ) {
            output.close();
          }
        AmazonS3 s3client = new AmazonS3Client(new ProfileCredentialsProvider());
        try {
        	System.out.println("Uploading a new object to S3 from a file\n");
            File file = new File(uploadFileName);
            s3client.putObject(new PutObjectRequest(
            		                 bucketName, keyName, file));

         } catch (AmazonServiceException ase) {
        	 System.out.println("Caught an AmazonServiceException, which " +
            		"means your request made it " +
                    "to Amazon S3, but was rejected with an error response" +
                    " for some reason.");
        	 System.out.println("Error Message:    " + ase.getMessage());
        	 System.out.println("HTTP Status Code: " + ase.getStatusCode());
        	 System.out.println("AWS Error Code:   " + ase.getErrorCode());
        	 System.out.println("Error Type:       " + ase.getErrorType());
        	 System.out.println("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
        	System.out.println("Caught an AmazonClientException, which " +
            		"means the client encountered " +
                    "an internal error while trying to " +
                    "communicate with S3, " +
                    "such as not being able to access the network.");
        	System.out.println("Error Message: " + ace.getMessage());
        }
    }
}
